
class Pessoa:
    def __init__(self, nome, agenda):
        self._nome = nome
        self._agenda = agenda

class Agenda:
    def __init__(self):
        self._lista_de_contatos = []

    def adicionar_contato(self, contato):
        self._lista_de_contatos.append(contato)


class Contato:
    def __init__(self, nome, telefone):
        self._nome = nome
        self._telefone = telefone

    def __repr__(self):
        return f'Nome: {self._nome}, Telefone: {self._telefone}'


contato01 = Contato('hbsis', 98765432)
contato02 = Contato('ambev', 12345678)
agenda = Agenda()

agenda.adicionar_contato(contato01)
agenda.adicionar_contato(contato02)

for contato in agenda:
    print(contato)


alessandro = Pessoa('Alessandro', agenda)

print(alessandro._agenda._lista_de_contatos)