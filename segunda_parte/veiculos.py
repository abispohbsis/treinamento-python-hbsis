from segunda_parte.employees import ReprMixin


class Carro(ReprMixin):
    def __init__(self, nome):
        self.nome = nome
        self._nivel_combustivel = 50

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, valor):
        if not isinstance(valor, str):
            raise Exception('O nome deve ser uma string')
        self._nome = valor

    def set_nivel_combustivel(self, valor):
        if valor == 0:
            raise Exception('Valor de combustível inválido')
        self._nivel_combustivel = valor

    def get_nivel_combustivel(self):
        return self._nivel_combustivel


fusca = Carro('Fusca')
print(fusca)

#fusca.set_nivel_combustivel(0)
print(fusca.get_nivel_combustivel())
print(fusca.nome)

#gol = Carro(453465)