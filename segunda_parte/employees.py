
from abc import ABC, abstractmethod


class ReprMixin:
    def __repr__(self):
        return f'<Nome: {self.__class__.__name__}>'


class Employee(ABC):
    def __init__(self, name: str, salary: float):
        super().__init__()
        self._name = name
        self._salary = salary

    @abstractmethod
    def calculate_salary(self):
        pass


class ComissionedEmployee(Employee, ReprMixin):
    def __init__(self, name, salary, value, comission):
        super().__init__(name, salary)
        self._value = value
        self._comission = comission

    def calculate_salary(self):
        return self._salary + (self._value * (self._comission / 100))


#alessandro = Employee('Alessandro', 1000.00)
#print(alessandro.calculate_salary())

ambev = ComissionedEmployee('Ambev', 600, 245, 10)
print(ambev)
print(ambev.calculate_salary())
del ambev