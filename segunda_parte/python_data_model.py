
class Contato:
    def __init__(self, nome: str, telefone: str):
        self._nome = nome
        self._telefone = telefone

    def info(self):
        print(f'Nome: {self._nome}, Telefone: {self._telefone}')

    def __repr__(self):
        return f'<Contato nome={self._nome} telefone={self._telefone}>'


class ListaDeContatos:
    def __init__(self):
        self._contatos = []
        self._counter = -1

    @property
    def contatos(self):
        return self._contatos

    def adicionar_contato(self, contato: Contato):
        self._contatos.append(contato)

    def __len__(self):
        return len(self._contatos)

    def __iter__(self):
        return self

    def __next__(self):
        self._counter += 1

        if self._counter < len(self._contatos):
            return self._contatos[self._counter]
        else:
            raise StopIteration

    def __getitem__(self, item):
        return self._contatos[item]

    def __setitem__(self, key, value):
        self._contatos[key] = value


joao = Contato("João", "554778394332")
jose = Contato("José", "554778394564")
maria = Contato("Maria", "551175794332")

lista_de_contatos = ListaDeContatos()
lista_de_contatos.adicionar_contato(joao)
lista_de_contatos.adicionar_contato(jose)
lista_de_contatos.adicionar_contato(maria)

print(len(lista_de_contatos))

for contato in lista_de_contatos:
    contato.info()

print(lista_de_contatos[0])
lista_de_contatos[0] = 10
print(lista_de_contatos[0])

# numeros = [1, 2, 3, 4, 5]
# for numero in numeros:
#     print(numero)

