
# class Pessoa:
#     geral = 0
#
#     def __init__(self, nome, funcao=''):
#         self._nome = nome
#         self._funcao = funcao
#         self._is_valid()
#
#     @property
#     def funcao(self):
#         return self._funcao
#
#     @funcao.setter
#     def funcao(self, nova_funcao):
#         self._funcao = nova_funcao
#
#     def get_nome(self):
#         return self._nome
#
#     def set_nome(self, nome):
#         self._nome = nome
#
#     def info(self):
#         return f'{self._nome}'
#
#     def _is_valid(self):
#         if not isinstance(self._nome, str):
#             raise Exception("Nome inválido")
#
#         return True
#
# pessoa01 = Pessoa('Pessoa01', 'Engenheiro')
# pessoa01.set_nome('Gato')
# #print(pessoa01.info())
# print(pessoa01.get_nome())
# print(pessoa01.funcao)
# pessoa01.funcao = 'Programador'
# print(pessoa01.funcao)


# class Contato:
#     def __init__(self, nome, endereco, telefone):
#         self.nome = nome
#         self._endereco = endereco
#         self._telefone = telefone
#         #self._is_valid()
#
#         self._validators = [self._is_nome_valido, self._is_telefone_valido, self._is_endereco_valido]
#
#         if not all([func() for func in self._validators]):
#             raise Exception("Erro")
#
#
#     @property
#     def nome(self):
#         return self._nome
#
#
#     @nome.setter
#     def nome(self, novo_nome):
#         self._nome = novo_nome
#         # if not self._is_nome_valido():
#         #     raise Exception("Nome não é válido")
#
#
#     def _is_nome_valido(self):
#         if not isinstance(self._nome, str):
#             return False
#         elif len(self._nome) < 10:
#             return False
#         else:
#             return True
#
#     def _is_endereco_valido(self):
#         if not isinstance(self._endereco, str):
#             return False
#         elif not self._endereco.upper().startswith(('RUA', 'AVENIDA')):
#             return False
#
#         return True
#
#     def _is_telefone_valido(self):
#         return isinstance(self._telefone, int)
#
#     def _is_valid(self):
#         # if not self._is_nome_valido():
#         #     raise Exception('Nome não é válido')
#
#         if not self._is_endereco_valido():
#             raise Exception('Endereço não é válido')
#
#         if not self._is_telefone_valido():
#             raise Exception('Telefone não é válido')
#
#
# contato01 = Contato(2323, "avenida 7 de setembro", 2323)

class InfoMixin:
    def info(self):
        return f'Classe: {self.__class__.__name__}, Atributos: {[atributo for atributo in self.__dict__]}'

class Foguete(InfoMixin):
    def __init__(self):
        self._modelo = 'Sputnik'
        self._fabricante = 'URSS'
        self._tamanho = 'Pequeno'

sputnik = Foguete()
print(sputnik.info())


class AnimalInterface:
    def falar(self):
        raise NotImplementedError("Esse método deve ser implementado")

class Animal(AnimalInterface, InfoMixin):
    def __init__(self, nome):
        self._nome = nome
        self._mamifero = True
        self._quadrupede = True

    def andar(self):
        return f'O {self.__class__.__name__} está andando.'

    def dormir(self):
        return f'O {self.__class__.__name__} está dormindo.'

    def __repr__(self):
        return self._nome


class Cachorro(Animal):
    def __init__(self, nome):
        super().__init__(nome)

    def falar(self):
        return "Au au."


class Gato(Animal):
    def __init__(self, nome):
        super().__init__(nome)

    def falar(self):
        return "Miau."


# scooby = Cachorro()
# print(scooby.andar())
# print(scooby.dormir())
# print(scooby.falar())
# print(scooby.info())
#
# print("\n")
# chica = Gato()
# print(chica.andar())
# print(chica.dormir())
# print(chica.falar())

from typing import List


class PetShop:
    def __init__(self, animal: Animal):
        print(animal.falar())
        self._animais = [Gato('Chica'), Cachorro('Chico'), Cachorro('Scooby'), Cachorro('fsdfsd'), Cachorro('dsfsdf'), Gato('fgfd')]
        self._dono = object()

    def mostrar_animais(self):
        return self._animais


pet_shop = PetShop(234)
print(pet_shop._animais[0].info())


