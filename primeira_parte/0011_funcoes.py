def func01(**kwargs):
    for key, value in kwargs.items():
        print(f'Argumento: {key} | Valor: {value}')

#func01(nome={}, cidade='blumenau', curso='python')

#######################################################
# Unpacking

lista_valores = ['Curso Python', 'Blumenau']
# def info(nome_curso, cidade):
#     return f'{nome_curso} - {cidade}'


#print(info(*lista_valores))

########################################################

dict_valores = {'nome': 'Curso Python', 'cidade': 'Blumenau', 'empresa': 'hbsis'}

def info(nome, cidade, empresa, arg01=None):
    return f'Curso: {nome}, Cidade: {cidade}, Empresa: {empresa}'


#### Função recursiva

def factorial(n):
    if n == 1:
        return 1
    else:
        return n * factorial(n-1)

print(factorial(4))

#### Funções anônimas

# def quadrado():
#     return 2 ** 2
#
#
# print(quadrado())

quadrado = lambda: 2 ** 2

print(quadrado())

def menor_que_zero(x):
    return x < 0


######### filter
number_list = range(-5, 5)
less_than_zero = list(filter(lambda x: x < -3, number_list))
less_than_zero = list(filter(menor_que_zero, number_list))
print(less_than_zero)


######### map
items = [1, 2, 3, 4, 5]
squared = list(map(lambda x: x**2, items))

# items = [1, 2, 3, 4, 5]
# squared = []
# for i in items:
#     squared.append(i**2)

print(squared)
"""
 Escreva uma função python que encontre o maior número entre 3 números
 Escreva uma função que recebe dois argumentos: um nome e um salário. Se o segundo argumento não for passado, defina o valor padrão de 1000
 Escreva uma função python que some todos os números de uma lista
 Escreva uma função Python que recebe uma string e calcule o número de caracteres maiúsculos e mínúsculos
 Escreva uma função Pyton que recebe uma lista e retorna uma nova lista com elementos únicos
 Escreva uma função python que receba uma lista e retorne uma string dessa lista
 Escreva uma função Python que receba uma quantidade arbitrária de argumentos
 Escreva uma função Python onde ela é chamada passando um dicionário na forma **
 Escreva uma função anônima no python e a atribua a uma variável. Depois execute essa função.
 Usando filter, filtre uma lista retornando apenas numeros (use isinstance)
 
"""

mensagem = "nok"
mensagem = True if 5 < 0 else False
print(mensagem)