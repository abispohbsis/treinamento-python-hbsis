# Set
# Um set(conjunto) é uma coleção de itens não ordenados e não indexados. Em um set, cada elemento é único, ou seja, não há itens repetidos em um set.

# Inicializando um set

my_set = {1, 2, 3}
print(my_set)

my_set = set([4, 5, 6])
print(my_set)

# Alterando um set
# Apesar de set ser um tipo mutável, não podemos alterar um item usando indexação ou slicing.
# Para isso, usamos os métodos .add e .update

# .add adiciona um item ao set
my_set.add('a')
print(my_set)

# .update adiciona vários itens de uma vez ao set
my_set.update([1, 2, 3], {9, 65})
print(my_set)


# Removendo itens de um set
# Para isso, usamos os métodos .discard, .remove ou .pop

# .discard remove um item do set, se esse item não existem, o set continua igual
my_set.discard(65)
my_set.discard(600)
print(my_set)

# já o método .remove, produzirá um erro se o item a ser retirado do set não existir
#my_set.remove(600)
#print(my_set)

# Podemos também usar o método .pop pra retirar e retornar um item do set, porém como os itens não são ordenados, o item que será retirado é randômico.
print(my_set.pop())


# Métodos ou operações
# Um set é a representação de um conjunto matemático, então esse tipo de dados suporta operações do tipo união, intersecção, etc

# União

A = {1, 2, 3, 4, 5}
B = {4, 5, 6, 7, 8}

# | é o operador de união para sets
print(A | B)
print(A.union(B))

# Intersecção

A = {1, 2, 3, 4, 5}
B = {4, 5, 6, 7, 8}

# & é o operador de intersecção para sets
print(A & B)
