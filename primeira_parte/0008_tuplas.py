# Tupĺas
# Uma tupla é uma sequência de objetos imutáveis.

# Inicializando uma tupla
# Podemos usar a classe tuple

my_tuple = tuple()
my_tuple = tuple([1, 2, 3, 4])           # Criamos uma tupla a partir de um iterável, nesse caso, uma lista
print(my_tuple)

# Ou podemos usar diretamente a sintaxe de criação de tuplas
my_tuple = (1, 2, 3, 4,)
my_tuple = 1, 2, 3, 4,

my_tuple += (5, 6, 7,)

# Métodos de tupla

# count(x) - Retorna a quantidade de vezer que um determinado valor aparece na tupla
print((1, 2, 3, 3, 5,).count(3))

# index(x, start, end) - Procura na tupla pelo valor especificado e retorna o índice onde ele foi encontrado
print((3, 2, 4, 3, 5,).index(3))
print((3, 2, 4, 3, 5,).index(3, 2))