# Dicionários
# Um dicionário em Python é uma estrutura do tipo chave: valor

# Inicializando um dicionário
# Podemos usar a classe dict

import copy

dict01 = {}         # dicionário vazio
dict01 = dict([(1, 2,), (3, 4,)])
dict01 = dict(nome='hbsis', curso='python')
print(dict01)

print("="*20)

# Acessando um item do dicionário
print(dict01['nome'])           # A chave deve existir, do contrário é gerada a exceção KeyError
print(dict01.get('nome'))       # Não é gerada exceção se a chave não existir, além de ser possível definir um valor padrão caso a chave não exista
print(dict01.get('idade', 21))

print("="*20)

# Iterando sobre um dicionário

# Usando keys()                 # Padrão
print(dict01.keys())
for key in dict01.keys():
    print(key)

print("="*20)

# Usando .values()
print(dict01.values())
for value in dict01.values():
    print(value)

print("="*20)

# usando .items()
print(dict01.items())
for key, value in dict01.items():
    print(f"Key: {key} | Value: {value}")


# Adicionando e removendo itens de um dicionário
# Podemos criar uma nova chave no dicionário diretamente
dict01['idade'] = 15
print(dict01)

# Ou podemos utilizar o método .update
# O método update cria uma nova chave se ela não existir, ou atualiza com um novo valor se existir
dict01.update({'idade': 22})
print(dict01)

# Para removermos uma chave de um dicionário, podemos usar os seguintes métodos
# .pop(key) - remove do dicionário e retorna a chave especificada como argumento do método
dict01.pop('idade')

# .popitem() - remove o item que foi inserido por último no dicionário
dict01.popitem()

# del - remove o dicionário inteiro ou uma chave especificada
del dict01['nome']
print(dict01)
del dict01

# Copiar um dicionário shallow copy e deep copy
# Ao copiar dicionários, precisamos ficar atentos aos tipos de valores contidos no dicionário.

words = {
    "Hello": 56,
    "at": 23,
    "test": 43,
    "this": 43,
    "who": [56, 34, 44]
}

new_words = words.copy()
new_words['at'] = 200

print(words)
print(new_words)

print("="*20)
new_words['who'].append(100)

print(words)
print(new_words)

# Como vimos acima, usando apenas .copy() a referẽncia aos valores do dicionário words é copiada para o dicionário
# new_words. Isso é especialmente problemático quando estamos trabalhando com estruturas do tipo list ou dict. Para
# evitar isso, usamos o método .deepcopy(), que cria uma cópia do dicionário.
print("="*20)
new_words = copy.deepcopy(words)
new_words['who'].append(200)

print(words)
print(new_words)

# Assim como as listas, nos valores do dicionário podemos ter qualquer tipo de valor (int, str, list, callable, etc)

dict01 = {
    'key1': {
        'subkey1': [1, 2, 3]
    },
    'key2': [{'subkeylist1': 10, 'subkeylist2': 20}],
    'key3': lambda: 2 ** 2
}

# Métodos

# .clear - remove o conteúdo de um dicionário
dict01 = {'empresa': 'hbsis', 'curso': 'python'}
dict01.clear()
print(dict01)

# .fromkeys - cria um dicionário a partir de um iterável
dict01 = dict.fromkeys(['nome', 'idade'], True)
print(dict01)

# .setdefault() - retorna o valor da chave especificada. Se a chave não existe, a cria com o valor especificado
dict01 = {'empresa': 'hbsis', 'curso': 'python'}
print(dict01.setdefault('empresa2', 'ambev'))
print(dict01)