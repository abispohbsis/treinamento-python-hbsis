from users.models import User
from register.actions import register


def create_user(name, age):
    register('username', 'password')
    user = User(name, age)

    return user


def update_user(user: User, name: str, age: int):
    user.name = name
    user.age = age

    return user
