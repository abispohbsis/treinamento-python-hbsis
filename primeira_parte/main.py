# from funcoes import soma
from users.actions import create_user
from users import __version__, __author__, update_user

# def subtracao(num1, num2):
#     return num1 - num2

user = create_user('Alessandro', 33)
print(user.name, user.age)

user = update_user(user, 'Juarez', 60)
print(user.name, user.age)

print(__version__)
print(__author__)