# Listas
# Listas são coleções de itens heterogêneos

# Criando uma lista
lista = []
lista = list()
lista2 = [6, 7, 8]


# Adicionando itens de uma lista
lista.append(20)
lista.append('hbsis')
lista.insert(1, 'python')

lista[0] = 42

# Acessando os itens de uma lista
print(lista[0])
print(42 not in lista)

# Removendo itens de uma lista
#lista.remove('hbsis')
#print(lista.pop(0))
#del lista[0]

# Limpando uma lista
lista.clear()

# Copiando listas
lista = lista[:]
lista2.append(20)

# Concatenando listas
lista.extend(lista2)
lista.extend([5, 6, 7, 8, 9])

#lista_hbsis = []
#for letra in 'hbsis':
#    lista_hbsis.append(letra)

lista_hbsis = [item for item in 'hbsis']

# Métodos
lista = ['s', 't', 'a', 'w']
lista.sort()
print(lista)