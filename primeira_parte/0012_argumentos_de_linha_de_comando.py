import argparse

######################################################
parser = argparse.ArgumentParser()
parser.add_argument("filename", help="Caminho onde está o arquivo a ser processado.")
parser.add_argument("-i", "--info", help="Mostra informações sobre o arquivo.", action="store_true")
args = parser.parse_args()

_file = open(args.filename)

if args.info:
    print(_file.closed)

print(_file.read())


_file.close()
