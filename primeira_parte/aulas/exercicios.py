"""
 Write a Python function to find the Max of three numbers
 Escreva uma função que recebe dois argumentos: um nome e um salário. Se o segundo argumento não for passado, defina o valor padrão de 1000
 Write a Python function to sum all the numbers in a list
 Write a Python function that accepts a string and calculate the number of upper case letters and lower case letters
 Write a Python function that takes a list and returns a new list with unique elements of the first list
 Escreva uma função python que receba uma lista e retorne uma string dessa lista
 Escreva uma função Python que receba uma quantidade arbitrária de argumentos
 Escreva uma função Python onde ela é chamada passando um dicionário na forma **
 Escreva uma função anônima no python e a atribua a uma variável. Depois execute essa função.
 Usando filter, filtre uma lista retornando apenas numeros (use isinstance)
"""

def ex01(num1, num2, num3):
    return max([num1, num2, num3])

print(ex01(4, 10, 3))

def ex02(nome, salario=1000):
    return f'Nome: {nome}, Salário: {salario}'

print(ex02('Alessandro'))
print(ex02('Alessandro', 1500))

def ex03(lista):
    return sum(lista)

print(ex03([1, 2, 3, 4, 5]))

def ex04(texto):
    minusculas = len(list(filter(lambda x: x.islower(), texto)))
    maiusculas = len(list(filter(lambda x: x.isupper(), texto)))
    return f'Quantidade Maiúsculas: {maiusculas} | Quantidade Minúsculas: {minusculas}'

print(ex04('jjJOjqQjJQOS'))

def ex05(lista):
    return list(set(lista))

print(ex05([1, 5, 4, 4, 7, 7, 2, 2, 2]))


def ex06(lista):
    return ''.join(lista)

print(ex06(['a', 'b', 'c', 'd', 'e']))


def ex07(*args):
    return f'Quantidade de argumentos recebidos: {len(args)}'

print(ex07())


def ex08(arg1, arg2, arg3):
    return f'arg1: {arg1}, arg2: {arg2}, arg3: {arg3}'

dict01 = {'arg1': 1, 'arg2': 2, 'arg3': 3}

print(ex08(**dict01))

ex09 = lambda: 2 ** 2
print(ex09())


def ex10(lista):
    return list(filter(lambda x: isinstance(x, int), lista))


print(ex10([1, 67, 'a', 'sdfd', [5, 4], {}, ex09]))