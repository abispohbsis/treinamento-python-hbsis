

class ValorMuitoPequeno(Exception):
    def __init__(self, message='O valor é muito pequeno'):
        super().__init__(self, message)


import sys
import traceback
dict01 = {}

try:
    print(1/1)
    raise ValorMuitoPequeno('dfsdfsdf')
except Exception as e:
    #print(str(e))
    #print(traceback.print_exc())
    print(sys.exc_info())
    print(e)

finally:
    print("Erro tratado")